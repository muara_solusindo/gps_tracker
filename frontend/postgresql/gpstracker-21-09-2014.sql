--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: prcsavegpslocation(numeric, numeric, integer, integer, integer, timestamp without time zone, character varying, character varying, character varying, character varying, integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION prcsavegpslocation(numeric, numeric, integer, integer, integer, timestamp without time zone, character varying, character varying, character varying, character varying, integer, character varying, character varying) RETURNS timestamp without time zone
    LANGUAGE sql
    AS $_$

    INSERT INTO gpslocations (latitude, longitude, speed, direction, distance, gpsTime, locationMethod, userName, phoneNumber,  sessionID, accuracy, extraInfo, eventType)
   VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13);
   SELECT now()::timestamp;

$_$;


ALTER FUNCTION public.prcsavegpslocation(numeric, numeric, integer, integer, integer, timestamp without time zone, character varying, character varying, character varying, character varying, integer, character varying, character varying) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: gpslocations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE gpslocations (
    gpslocationid integer NOT NULL,
    lastupdate timestamp without time zone DEFAULT now() NOT NULL,
    latitude double precision DEFAULT 0::double precision NOT NULL,
    longitude double precision DEFAULT 0::double precision NOT NULL,
    phonenumber character varying(50) DEFAULT ''::character varying NOT NULL,
    username character varying(50) DEFAULT ''::character varying NOT NULL,
    sessionid character varying(50) DEFAULT ''::character varying NOT NULL,
    speed integer DEFAULT 0 NOT NULL,
    direction character varying(10) DEFAULT '0'::character varying,
    distance double precision DEFAULT 0::double precision NOT NULL,
    gpstime timestamp without time zone,
    locationmethod character varying(50) DEFAULT ''::character varying NOT NULL,
    accuracy integer DEFAULT 0 NOT NULL,
    extrainfo character varying(255) DEFAULT ''::character varying NOT NULL,
    eventtype character varying(50) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE gpslocations OWNER TO postgres;

--
-- Name: gpslocations_gpslocationid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE gpslocations_gpslocationid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gpslocations_gpslocationid_seq OWNER TO postgres;

--
-- Name: gpslocations_gpslocationid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE gpslocations_gpslocationid_seq OWNED BY gpslocations.gpslocationid;


--
-- Name: v_getallroutesformap; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_getallroutesformap AS
 SELECT gpslocations.sessionid,
    gpslocations.gpstime,
    concat('{ "latitude":"', (gpslocations.latitude)::character varying, '", "longitude":"', (gpslocations.longitude)::character varying, '", "speed":"', (gpslocations.speed)::character varying, '", "direction":"', gpslocations.direction, '", "distance":"', (gpslocations.distance)::character varying, '", "locationMethod":"', gpslocations.locationmethod, '", "gpsTime":"', to_char(gpslocations.gpstime, 'Mon dd YYYY HH12:MIAM'::text), '", "userName":"', gpslocations.username, '", "phoneNumber":"', gpslocations.phonenumber, '", "sessionID":"', gpslocations.sessionid, '", "accuracy":"', (gpslocations.accuracy)::character varying, '", "extraInfo":"', gpslocations.extrainfo, '" }') AS json
   FROM (( SELECT max(gpslocations_1.gpslocationid) AS id
           FROM gpslocations gpslocations_1
          WHERE ((((gpslocations_1.sessionid)::text <> '0'::text) AND (char_length((gpslocations_1.sessionid)::text) <> 0)) AND (gpslocations_1.gpstime <> NULL::timestamp without time zone))
          GROUP BY gpslocations_1.sessionid) maxid
     JOIN gpslocations ON ((gpslocations.gpslocationid = maxid.id)))
  ORDER BY gpslocations.gpstime;


ALTER TABLE v_getallroutesformap OWNER TO postgres;

--
-- Name: v_getrouteformap; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_getrouteformap AS
 SELECT gpslocations.sessionid,
    gpslocations.lastupdate,
    concat('{ "latitude":"', (gpslocations.latitude)::character varying, '", "longitude":"', (gpslocations.longitude)::character varying, '", "speed":"', (gpslocations.speed)::character varying, '", "direction":"', gpslocations.direction, '", "distance":"', (gpslocations.distance)::character varying, '", "locationMethod":"', gpslocations.locationmethod, '", "gpsTime":"', to_char(gpslocations.gpstime, 'Mon dd YYYY HH12:MIAM'::text), '", "userName":"', gpslocations.username, '", "phoneNumber":"', gpslocations.phonenumber, '", "sessionID":"', gpslocations.sessionid, '", "accuracy":"', (gpslocations.accuracy)::character varying, '", "extraInfo":"', gpslocations.extrainfo, '" }') AS json
   FROM gpslocations
  WHERE (gpslocations.gpstime IS NOT NULL)
  ORDER BY gpslocations.lastupdate;


ALTER TABLE v_getrouteformap OWNER TO postgres;

--
-- Name: v_getroutes; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_getroutes AS
 SELECT concat('{ "sessionID": "', routes.sessionid, '", "userName": "', routes.username, '", "times": "(', to_char(routes.starttime, 'Mon dd YYYY HH12:MIAM'::text), ' - ', to_char(routes.endtime, 'Mon dd YYYY HH12:MIAM'::text), ')" }') AS json
   FROM ( SELECT DISTINCT gpslocations.sessionid,
            gpslocations.username,
            min(gpslocations.gpstime) AS starttime,
            max(gpslocations.gpstime) AS endtime
           FROM gpslocations
          GROUP BY gpslocations.sessionid, gpslocations.username
          ORDER BY min(gpslocations.gpstime) DESC) routes;


ALTER TABLE v_getroutes OWNER TO postgres;

--
-- Name: gpslocationid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gpslocations ALTER COLUMN gpslocationid SET DEFAULT nextval('gpslocations_gpslocationid_seq'::regclass);


--
-- Data for Name: gpslocations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY gpslocations (gpslocationid, lastupdate, latitude, longitude, phonenumber, username, sessionid, speed, direction, distance, gpstime, locationmethod, accuracy, extrainfo, eventtype) FROM stdin;
4	2018-09-21 08:56:36.156683	3.63371600000000017	98.8789239999999978	864893031560967	tk103-user	1	0	0	0	2018-09-21 14:37:31		0		tk103
\.


--
-- Name: gpslocations_gpslocationid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('gpslocations_gpslocationid_seq', 4, true);


--
-- Name: gpslocations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY gpslocations
    ADD CONSTRAINT gpslocations_pkey PRIMARY KEY (gpslocationid);


--
-- Name: phonenumberindex; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX phonenumberindex ON public.gpslocations USING btree (phonenumber);


--
-- Name: sessionidindex; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sessionidindex ON public.gpslocations USING btree (sessionid);


--
-- Name: usernameindex; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX usernameindex ON public.gpslocations USING btree (username);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

