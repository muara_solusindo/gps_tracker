--------------------------------------------------------------
-- SQL for creating GpsTracker object in a PostgreSQL database
--------------------------------------------------------------
-- CREATE USER gpstracker_user WITH PASSWORD 'gpstracker';

DROP VIEW  IF EXISTS v_GetAllRoutesForMap;
DROP VIEW  IF EXISTS v_GetRouteForMap;
DROP VIEW  IF EXISTS v_GetRoutes;
DROP INDEX IF EXISTS sessionIDIndex;
DROP INDEX IF EXISTS phoneNumberIndex;
DROP INDEX IF EXISTS userNameIndex;

DROP TABLE IF EXISTS gpslocations;

CREATE TABLE gpslocations (
  GPSLocationID serial,
  lastUpdate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  latitude double precision NOT NULL DEFAULT '0.0',
  longitude double precision NOT NULL DEFAULT '0.0',
  phoneNumber varchar(50) NOT NULL DEFAULT '',
  userName varchar(50) NOT NULL DEFAULT '',
  sessionID varchar(50) NOT NULL DEFAULT '',
  speed integer  NOT NULL DEFAULT '0',
  direction varchar(10) NULL DEFAULT '0',
  distance double precision NOT NULL DEFAULT '0.0',
  gpsTime timestamp NULL,
  locationMethod varchar(50) NOT NULL DEFAULT '',
  accuracy integer  NOT NULL DEFAULT '0',
  extraInfo varchar(255) NOT NULL DEFAULT '',
  eventType varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (GPSLocationID)
);

CREATE INDEX sessionIDIndex   ON gpslocations (sessionID);
CREATE INDEX phoneNumberIndex ON gpslocations (phoneNumber);
CREATE INDEX userNameIndex    ON gpslocations (userName);


-----------------------------------------------
--   v_GetAllRoutesForMap --
-----------------------------------------------
CREATE OR REPLACE VIEW v_getallroutesformap AS 
 SELECT gpslocations.sessionid,
    gpslocations.gpstime,
    concat('{ "latitude":"', gpslocations.latitude::character varying, '", "longitude":"', gpslocations.longitude::character varying, '", "speed":"', gpslocations.speed::character varying, '", "direction":"', gpslocations.direction::character varying, '", "distance":"', gpslocations.distance::character varying, '", "locationMethod":"', gpslocations.locationmethod, '", "gpsTime":"', to_char(gpslocations.gpstime, 'Mon dd YYYY HH12:MIAM'::text), '", "userName":"', gpslocations.username, '", "phoneNumber":"', gpslocations.phonenumber, '", "sessionID":"', gpslocations.sessionid, '", "accuracy":"', gpslocations.accuracy::character varying, '", "extraInfo":"', gpslocations.extrainfo, '" }') AS json
   FROM ( SELECT max(gpslocations_1.gpslocationid) AS id
           FROM gpslocations gpslocations_1
          WHERE gpslocations_1.sessionid::text <> '0'::text AND char_length(gpslocations_1.sessionid::text) <> 0 AND gpslocations_1.gpstime <> null
          GROUP BY gpslocations_1.sessionid) maxid
     JOIN gpslocations ON gpslocations.gpslocationid = maxid.id
  ORDER BY gpslocations.gpstime;


-----------------------------------------------
--   v_GetRouteForMap --
-----------------------------------------------
CREATE Or REPLACE VIEW v_GetRouteForMap AS
SELECT 
  sessionid, 
  lastupdate,
  CONCAT('{ "latitude":"', CAST(latitude AS VARCHAR),'", "longitude":"', CAST(longitude AS VARCHAR), '", "speed":"', CAST(speed AS VARCHAR), '", "direction":"', CAST(direction AS VARCHAR), '", "distance":"', CAST(distance AS VARCHAR), '", "locationMethod":"', locationMethod, '", "gpsTime":"', to_char(gpsTime, 'Mon dd YYYY HH12:MIAM'), '", "userName":"', userName, '", "phoneNumber":"', phoneNumber, '", "sessionID":"', CAST(sessionID AS VARCHAR), '", "accuracy":"', CAST(accuracy AS VARCHAR), '", "extraInfo":"', extraInfo, '" }') json
  FROM gpslocations
  WHERE gpstime is not null
  ORDER BY lastupdate;
 ;

-----------------------------------------------
--   v_GetRoutes --
-----------------------------------------------
CREATE OR REPLACE VIEW v_GetRoutes AS
select 
    CONCAT('{ "sessionID": "', CAST(sessionID AS VARCHAR),  '", "userName": "', userName, '", "times": "(', to_char(startTime, 'Mon dd YYYY HH12:MIAM'), ' - ', to_char(endtime, 'Mon dd YYYY HH12:MIAM'), ')" }') json
from (
  select 
    distinct sessionid, userName, 
	MIN(gpsTime) startTime, 
	MAX(gpsTime) endtime
 FROM gpslocations
 group by sessionid,username
 ORDER BY startTime DESC
)
AS routes
;

 
